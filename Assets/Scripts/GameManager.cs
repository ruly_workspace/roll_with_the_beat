﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.Text;
using System.IO;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

    public GameObject[,] grid;
    public GameObject tile;
    public int columns;
    public int rows;
    public Transform originPoint;
    public int levelNum;

    public AudioClip levelMusic;
    public AudioClip createMusic;

    public GameObject player;
    public GameObject goal;

    public GameObject modeButton;
    public Sprite createSprite;
    public Sprite rollSprite;

    private int currentColumn;

    public AudioClip[] instruments;

    public bool createMode;
    
    private Transform playerOrigin;

    public float tickSpeed;

    // Use this for initialization
    void Start () {
        grid = new GameObject[rows, columns];
        createGrid(grid);
        LoadLevel();
        currentColumn = columns-1;
        createMode = true;
        
    }
	
	// Update is called once per frame
	void Update () {
        
	}

    private void createGrid(GameObject[,] grid)
    {
        for(int i = 0; i < rows; i++)
        {
            for(int j = 0; j < columns; j++)
            {
                GameObject tile = Instantiate(this.tile);
                tile.transform.position = new Vector3(originPoint.position.x + 0.64f*j, originPoint.position.y + 0.64f * i, 0);
                tile.GetComponent<Tile>().gameManager = this.gameObject;
                tile.GetComponent<Tile>().row = (float)i;
                if (instruments[i] != null) {
                    tile.GetComponent<AudioSource>().clip = instruments[i];
                }
                grid[i, j] = tile;


            }
        }
    }

    void Tick()
    {
        /*
        for (int i = 0; i < rows; i++)
        {
            grid[i, currentColumn].GetComponent<SpriteRenderer>().color = Color.white;
        }*/

        if (currentColumn == columns)
        {
            currentColumn = 0;
        }

        // Here goes the part where everything happens
        for (int i = 0; i < rows; i++)
        {
            if (grid[i, currentColumn].GetComponent<Tile>().activated)
            {
                grid[i, currentColumn].GetComponent<AudioSource>().Play();
                grid[i, currentColumn].transform.GetChild(0).GetComponent<Animator>().Play("speaker");
            }
        }

        currentColumn++;

        
    }

    public void ChangeMode()
    {
        if(createMode)
        {
            Debug.Log("Cambiadito");
            createMode = false;
            modeButton.GetComponent<Image>().sprite = rollSprite;
            player.GetComponent<Rigidbody2D>().simulated = true;
            currentColumn = 0;
            InvokeRepeating("Tick", 0, tickSpeed);
            this.GetComponent<AudioSource>().Stop();
            this.GetComponent<AudioSource>().clip = levelMusic;
            this.GetComponent<AudioSource>().Play();
        } else
        {
            Debug.Log("Cambiadito");
            createMode = true;
            modeButton.GetComponent<Image>().sprite = createSprite;
            player.GetComponent<Rigidbody2D>().simulated = false;
            CancelInvoke();
            this.GetComponent<AudioSource>().Stop();
            this.GetComponent<AudioSource>().clip = createMusic;
            this.GetComponent<AudioSource>().Play();
        }
    }

    public void respawn()
    {
        player.transform.position = playerOrigin.position;
    }

    public void nextLevel()
    {
        if(levelNum != 6)
        {
            SceneManager.LoadScene("Level" + (levelNum + 1));
        } else
        {
            SceneManager.LoadScene("Ending");
        }
        
    }

    private bool LoadLevel()
    {
        int row = rows-1;
        try
        {
            string line;
            StreamReader theReader = new StreamReader(Application.dataPath + "/StreamingAssets/Level"+this.levelNum+".txt");
            using (theReader)
            {
                // While there's lines left in the text file, do this:
                do
                {
                    
                    line = theReader.ReadLine();

                    if (line != null)
                    {
                        string[] entries = line.Split(',');
                        if (entries.Length > 0)
                        {
                            for (int i = 0; i < entries.Length; i++)
                            {
                                if(entries[i] == "x")
                                {
                                    grid[row, i].GetComponent<Tile>().MarkAsLevelTile();
                                    
                                } else if(entries[i] == "p")
                                {
                                    player.transform.position = grid[row, i].transform.position;
                                    playerOrigin = grid[row, i].transform;
                                    
                                } else if(entries[i] == "g")
                                {
                                    goal.transform.position = grid[row, i].transform.position;
                                }
                                
                            }
                            row--;
                        }
                    }
                }
                while (line != null);
                theReader.Close();
                return true;
            }
        }
        catch (System.Exception e)
        {
            Debug.Log("{0}\n" + e.Message);
            return false;
        }
    }

    public void backToMenu()
    {
        SceneManager.LoadScene("Menu");
    }
}
