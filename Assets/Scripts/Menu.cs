﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

    public GameObject HowToPanel;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void openHowToPanel()
    {
        HowToPanel.SetActive(true);
    }

    public void closeHowToPanel()
    {
        HowToPanel.SetActive(false);
    }

    public void exit()
    {
        Application.Quit();
    }

    public void start()
    {
        SceneManager.LoadScene("Level1");
    }
}
