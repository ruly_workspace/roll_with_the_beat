﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    [HideInInspector] public bool facingRight = true;
    [HideInInspector] public bool jump = false;
    public float moveForce = 365f;
    public float maxSpeed = 5f;
    public float jumpForce = 1000f;
    public Transform groundCheck;


    private bool grounded = true;
    private Animator anim;
    private Rigidbody2D rb2d;

    public GameManager manager;

    // Use this for initialization
    void Awake()
    {
        //anim = GetComponent<Animator>();
        rb2d = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        //grounded = Physics2D.Linecast(transform.position, groundCheck.position, 8 << LayerMask.NameToLayer("Ground"));

        if (Input.GetButtonDown("Jump") && grounded)
        {
            jump = true;
            this.GetComponent<Animator>().SetBool("isJumping", true);
        }
    }

    void FixedUpdate()
    {
        if(!manager.createMode)
        {
            float h = Input.GetAxis("Horizontal");

            //anim.SetFloat("Speed", Mathf.Abs(h));

            if (h * rb2d.velocity.x != 0)
            {
                this.GetComponent<Animator>().SetBool("isRunning", true);
            }
            else
            {
                this.GetComponent<Animator>().SetBool("isRunning", false);
            }

            if (h * rb2d.velocity.x < maxSpeed)
                rb2d.AddForce(Vector2.right * h * moveForce);

            if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
                rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);

            if (h > 0 && !facingRight)
                Flip();
            else if (h < 0 && facingRight)
                Flip();

            if (jump)
            {
                // anim.SetTrigger("Jump");
                rb2d.AddForce(new Vector2(0f, jumpForce));
                jump = false;
                grounded = false;
            }
        }
    }


    void Flip()
    {
        facingRight = !facingRight;
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log(collision.gameObject.name);
        if(!grounded && collision.collider.sharedMaterial == null)
        {
            grounded = true;
            this.GetComponent<Animator>().SetBool("isJumping", false);
            this.transform.rotation = new Quaternion(0f,0f,0f,0f);
        }
    }
}
