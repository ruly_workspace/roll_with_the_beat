﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tile : MonoBehaviour {

    public bool activated;
    public bool levelTile;
    public GameObject gameManager;
    public float row;

	// Use this for initialization
	void Awake () {
        activated = false;
        levelTile = false;
        this.GetComponent<SpriteRenderer>().color = Color.grey;
        gameManager = GameObject.FindGameObjectWithTag("GameManager");

    }
	
	// Update is called once per frame
	void Update () {
        if (Input.GetMouseButtonDown(0) && gameManager.GetComponent<GameManager>().createMode)
        {
            Vector3 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector2 mousePos2D = new Vector2(mousePos.x, mousePos.y);

            RaycastHit2D hit = Physics2D.Raycast(mousePos2D, Vector2.zero);
            if (hit.collider != null && hit.collider == this.GetComponent<BoxCollider2D>() && !activated && !levelTile)
            {
                for(int i = 1; i < this.GetComponents<BoxCollider2D>().Length; i++)
                {
                    this.GetComponents<BoxCollider2D>()[i].isTrigger = false;
                }
                activated = true;
                this.gameObject.layer = 8;
                float blue = 255f - (row * 25f);
                this.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, blue/255f);
            } else if (hit.collider != null && hit.collider == this.GetComponent<BoxCollider2D>() && activated && !levelTile)
            {
                for (int i = 1; i < this.GetComponents<BoxCollider2D>().Length; i++)
                {
                    this.GetComponents<BoxCollider2D>()[i].isTrigger = true;
                }
                activated = false;
                this.gameObject.layer = 0;
                this.GetComponent<SpriteRenderer>().color = Color.grey;
            }

        }
    }
    
    public void MarkAsLevelTile()
    {
        
        this.activated = true;
        this.levelTile = true;
        for (int i = 1; i < this.GetComponents<BoxCollider2D>().Length; i++)
        {
            this.GetComponents<BoxCollider2D>()[i].isTrigger = false;
        }
        this.gameObject.layer = 8;
        float blue = 255f - (row * 25f);
        this.GetComponent<SpriteRenderer>().color = new Color(0f, 1f, blue / 255f);
    }

}
